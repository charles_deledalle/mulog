function h = plotimageinsar(img, type, varargin)
%% Display an InSAR image
%
% Input/Output
%
%    img        a 2 x 2 x M x N matrix field
%
%    type       'amplitude': display the amplitude
%               'phase':     display the phase
%               'coherence': display the coherence
%
%    h          handle on the created axes
%
% Optional arguments
%
%    see plotimage
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle
% Email charles-alban.deledalle@math.u-bordeaux.fr



[D, D, n1, n2] = size(img);

if D ~= 2
    error(['this function should plot insar images (2x2 cov matrices)']);
end
switch type
    case 'amplitude'
        img = squeeze(img(1, 1, :, :) + img(2, 2, :, :)) / 2;
        h = plotimage(squeeze(sqrt(real(img))), 'Adjust', 'm3s', varargin{:});
    case 'phase'
        img = squeeze(angle(img(1, 2, :, :)));
        h = plotimage(abs(img), 'range', [0 pi]);
    case 'coherence'
        img = squeeze(2 * abs(img(1, 2, :, :)) ./ ...
                      (img(1, 1, :, :) + img(2, 2, :, :)));
        h = plotimage(img, 'range', [0 1]);
end

if nargout == 0
    clear h;
end

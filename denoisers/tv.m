function x = tv(y, sig, varargin)
%% Wraper to my Fortran90 based TV implementation
%
% Input/Ouput:
%
%    Y          the observation (size MxNxK)
%
%    X          the solution (size MxNxK)
%
%    SIG        the standard deviation of the noise
%
%
% Reference
%
%    Rudin, Leonid I., Stanley Osher, and Emad Fatemi,
%    Nonlinear total variation based noise removal algorithms,
%    Physica D: Nonlinear Phenomena 60.1-4 (1992): 259-268.
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle
% Email charles-alban.deledalle@math.u-bordeaux.fr



if ~strcmp(mexext, 'mexa64')
    error(['This TV implementation is only compatible with Linux ' ...
           '64bits']);
end

options   = makeoptions(varargin{:});
[M, N, D] = size(y);
if D > 1
    parfor k=1:D
        out{k} = tv(y(:,:,k), sig, varargin{:});
    end
    x = zeros(M, N, D);
    for k=1:D
        x(:,:,k) = out{k};
    end
    return
end

op_id     = op_create('id', M, N);

normtype  = getoptions(options, 'normtype', 'iso');
op        = getoptions(options, 'op', op_id);
ima_init  = getoptions(options, 'ima_init', []);

T         = getoptions(options, 'tvT', 1000);
tvmethod  = getoptions(options, 'tvmethod', 'dr');

x = tv_interface(y, 0.7*sig, T, op, ...
                 tvmethod, normtype, ...
                 ima_init);

op_free(op_id);

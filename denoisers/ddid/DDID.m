function x = DDID(y, sigma2)
%% Original implementation of DDID
%
%    Knaus, Claude, and Matthias Zwicker.
%    Dual-domain image denoising." Image Processing (ICIP), 2013
%    20th IEEE International Conference on. IEEE, 2013.


x = step(y, y, sigma2, 15, 7, 100, 4.0);
x = step(x, y, sigma2, 15, 7, 8.7, 0.4);
x = step(x, y, sigma2, 15, 7, 0.7, 0.8);

end

function xt = step(x, y, sigma2, r, sigma_s, gamma_r, gamma_f)
    [dx dy] = meshgrid(-r:r);
    h = exp(- (dx.^2 + dy.^2) / (2 * sigma_s^2));
    xp = padarray(x, [r r], 'symmetric');
    yp = padarray(y, [r r], 'symmetric');
    xt = zeros(size(x));

    for p = 1:numel(x), [i j] = ind2sub(size(x), p);

        % Spatial Domain: Bilateral Filter
        g = xp(i:i+2*r, j:j+2*r);
        y = yp(i:i+2*r, j:j+2*r);
        d = g - g(1+r, 1+r);
        k = exp(- d.^2 ./ (gamma_r * sigma2)) .* h;
        gt = sum(sum(g .* k)) / sum(k(:));
        st = sum(sum(y .* k)) / sum(k(:));

        V = sigma2 .* sum(k(:).^2);
        G = fft2(ifftshift((g - gt) .* k));
        S = fft2(ifftshift((y - st) .* k));
        K = exp(- gamma_f * V ./ (G .* conj(G)));
        St = sum(sum(S .* K)) / numel(K);

        xt(p) = st + real(St);
    end
end

clear all
close all

addpathrec('.');
deterministic('on');

%% Setting
sig = 20;
denoiser = @bm3d;

%% Gaussian noise simulation setting
x = loadimage('data/cameraman.png');
y = x + sig * randn(size(x));

%% Run ADMM with embedded Gaussian denoiser
disp('Run ADMM with embedded Gaussian denoiser');
tic;
h = robustwaitbar(0);
xhat = admm(y, denoiser, 'sig', sig, ...
            'waitbar', @(p) robustwaitbar(p, h));
close(h);
disp(sprintf('  Elapsed time %.2f s', toc));

%% Run Gaussian denoiser only
disp('Run Gaussian denoiser only');
tic
xbm3d = bm3d(y, sig);
disp(sprintf('  Elapsed time %.2f s', toc));

%% Display results
f = fancyfigure;
subplot(1, 3, 1);
plotimage(y);
title(sprintf('Noisy image (psnr %.2f)', psnr(y, x)));
subplot(1, 3, 2);
plotimage(xhat);
title(sprintf('BM3D+ADMM (psnr %.2f)', psnr(xhat, x)));
subplot(1, 3, 3);
plotimage(xbm3d);
title(sprintf('BM3D only (psnr %.2f)', psnr(xbm3d, x)));
linkaxes

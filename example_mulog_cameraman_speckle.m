clear all
close all

addpathrec('.');
deterministic('on');

%% Setting
L = 2;
denoiser = @bm3d;

%% Gaussian noise simulation setting
x = loadimage('data/cameraman.png').^2;
[m, n] = size(x);
y = x .* mean((randn(m, n, L).^2 + randn(m, n, L).^2) / 2, 3);

%% Run MuLoG with embedded BM3D Gaussian denoiser
disp('Run MuLoG with embedded BM3D Gaussian denoiser');
tic;
h = robustwaitbar(0);
xhat = mulog(y, L, denoiser, ...
            'waitbar', @(p) robustwaitbar(p, h));
close(h);
disp(sprintf('  Elapsed time %.2f s', toc));

%% Run Homomorphic BM3D only
disp('Run Homomorphic BM3D only');
tic
xbm3d = exp(denoiser(log(y), sqrt(psi(1, L))) + log(L) - psi(L));
disp(sprintf('  Elapsed time %.2f s', toc));

%% Display results
f = fancyfigure;
subplot(1, 3, 1);
h = plotimagesar(y, 'range', [0 255]);
title(sprintf('Noisy image (psnr %.2f)', perfs(y, x, L, 'psnr')));
subplot(1, 3, 2);
plotimagesar(xhat, 'rangeof', h);
title(sprintf('MuLoG+BM3D (psnr %.2f)', perfs(xhat, x, L, 'psnr')));
subplot(1, 3, 3);
plotimagesar(xbm3d, 'rangeof', h);
title(sprintf('Homomorphic BM3D (psnr %.2f)', perfs(xbm3d, x, L, 'psnr')));
linkaxes

clear all
close all

addpathrec('.');

%% Load a polsar image
C = loadsarimg('data/tsukaba_pisar.mat');

%% Setting
L            = 2;       % input number of looks
R            = 1;       % input rank

%% Run MuLoG
h = robustwaitbar(0);

%% ... with TV
disp('Run MuLoG with embedded TV');
tic;
Shat_tv = ...
    mulog(C, L, @tv, ...
          'waitbar', @(p) robustwaitbar(p/2, h), ...
          'R', R);
disp(sprintf('  Elapsed time %.2f s', toc));

%% ... with BM3D
disp('Run MuLoG with embedded BM3D');
tic;
Shat_bm3d = ...
    mulog(C, L, @bm3d, ...
          'waitbar', @(p) robustwaitbar(p/2 + 1/2, h), ...
          'R', R);
disp(sprintf('  Elapsed time %.2f s', toc));

close(h);

%% Display results
close all
f = fancyfigure;
subplot(1, 3, 1);
h = plotimagesar(C, 'beta', 3, 'alpha', 0.7, 'adjust', 'm2s');
title('Input SAR image');
subplot(1, 3, 2);
plotimagesar(Shat_tv, 'rangeof', h);
title('MuLoG with TV');
subplot(1, 3, 3);
plotimagesar(Shat_bm3d, 'rangeof', h);
title('MuLoG with BM3D');
linkaxes;

%% Save figure
%savesubfig(f, '/tmp/result');
